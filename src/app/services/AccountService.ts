import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {ApiService} from './ApiService';
import {Account} from '../models/Account';
import {HttpClient, HttpParams} from '@angular/common/http';
import {SortDir} from '../models/SortDir';
import {Observable} from 'rxjs/index';
import {Balance} from '../models/Balance';

@Injectable()
export class AccountService {
  /* cache */
  private accountsByName: Map<String, Account> = new Map<String, Account>();
  private accountsById: Map<number, Account> = new Map<number, Account>();
  private balances: Map<number, Balance[]> = new Map<number, Balance[]>();

  constructor(private http: HttpClient) {
  }

  public getByName(name: string): Promise<Account> {
    if (this.accountsByName.has(name)) {
      return new Promise<Account>((resolve) => {
        resolve(this.accountsByName.get(name));
      });
    } else {
      return this.fetch(name);
    }
  }

  public getById(id: number): Promise<Account> {
    // TypeScript workaround, ID is supposed to be number although it often
    // comes in as a string which map wont recognize when using has() and get()
    id = parseInt(String(id));

    if (this.accountsById.has(id)) {
      return new Promise<Account>((resolve) => {
        resolve(this.accountsById.get(id));
      });
    } else {
      return this.fetch(id);
    }
  }

  public fetch(account: number | string): Promise<Account> {
    return new Promise<Account>((resolve, reject) => {
      this.http.get(ApiService.API_ENDPOINT + '/accounts/' + account).pipe(map(
        response => <Account>response
      )).subscribe(a => {
        this.accountsByName.set(a.name, a);
        this.accountsById.set(a.id, a);
        resolve(a);
      }, e => {
        reject(e);
      });
    });
  }

  public getAccountBalances(accountId: number): Promise<Balance[]> {
    return new Promise<Balance[]>((resolve, reject) => {
      if (!this.balances.has(accountId)) {
        return this.http.get(ApiService.API_ENDPOINT + '/accounts/' + accountId + '/balances').pipe(map(
          response => <Balance[]>response
        )).subscribe(balances => {
          this.balances.set(accountId, balances);
          resolve(balances);
        }, error => {
          reject(error);
        });
      } else {
        resolve(this.balances.get(accountId));
      }
    });
  }

  public getAccounts(
    limit: number,
    offset: number,
    sortBy: string = 'id',
    sortDir: SortDir = SortDir.ASC
  ): Promise<Account[]> {
    return new Promise<Account[]>((resolve, reject) => {
      this.http.get(ApiService.API_ENDPOINT + '/accounts', {
        params: new HttpParams()
          .set('sortBy', sortBy)
          .set('sortDir', String(sortDir))
          .set('limit', String(limit))
          .set('offset', String(offset))
      }).pipe(map(
        response => <Account[]>response
      )).subscribe(accounts => {
        accounts.forEach(account => {
          this.accountsById.set(account.id, account);
          this.accountsByName.set(account.name, account);
        });
        resolve(accounts);
      }, error => {
        reject(error);
      });
    });
  }

  public getAccountCount(): Observable<number> {
    return this.http.get(ApiService.API_ENDPOINT + '/accounts/count').pipe(map(
      response => (<any>response).count
    ));
  }
}
