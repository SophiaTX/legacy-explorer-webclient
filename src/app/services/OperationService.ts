import {Injectable} from '@angular/core';
import {AccountsPair} from '../models/AccountsPair';
import {OperationType} from '../models/OperationType';
import {NumberRange} from '../models/NumberRange';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Operation} from '../models/Operation';
import {ApiService} from './ApiService';
import {SortDir} from '../models/SortDir';
import {Observable} from 'rxjs/index';

@Injectable()
export class OperationService {
  constructor(private http: HttpClient) {
  }

  public countOperations(
    accounts: AccountsPair = null,
    types: OperationType[] = null,
    blockHeight: NumberRange = null
  ): Observable<number> {
    // TODO: there are problems with this on Firefox when `type` param is used alone
    const params = OperationService.buildFilterParams(accounts, types, blockHeight);

    return this.http.get(ApiService.API_ENDPOINT + '/operations/count', {
      params: params
    }).pipe(map(
      response => (<any>response).count
    ));
  }

  public getOperations(
    limit: number,
    offset: number,
    sortDir: SortDir = SortDir.DESC,
    accounts: AccountsPair = null,
    types: OperationType[] = null,
    blockHeight: NumberRange = null
  ): Observable<Operation[]> {
    const params = OperationService.buildFilterParams(accounts, types, blockHeight)
      .set('sortDir', sortDir)
      .set('offset', String(offset))
      .set('limit', String(limit));

    return this.http.get(ApiService.API_ENDPOINT + '/operations', {
      params: params
    }).pipe(map(
      response => <Operation[]> response
    ));
  }

  private static buildFilterParams(accounts, types, blockHeight) {
    let params = new HttpParams();

    if (accounts !== null) {
      if (accounts.sender !== null) {
        params = params.set('senderId', String(accounts.sender.id));
      }

      if (accounts.target !== null) {
        params = params.set('targetId', String(accounts.target.id));
      }

      if (
        accounts.sender !== null &&
        accounts.target !== null &&
        accounts.operator !== null
      ) {
        params = params.set('operator', String(accounts.operator));
      }
    }

    if (types !== null) {
      if (types.length > 0) {
        params = params.set('type', types.join(','));
      }
    }

    if (blockHeight != null) {
      if (blockHeight.from !== null && blockHeight.to !== null) {
        params = params.set('blockHeight', blockHeight.from + '-' + blockHeight.to);
      } else if (blockHeight.from !== null) {
        params = params.set('blockHeight', blockHeight.from);
      } else if (blockHeight.to !== null) {
        params = params.set('blockHeight', blockHeight.to);
      }
    }

    return params;
  }
}
