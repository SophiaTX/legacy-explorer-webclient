import {Injectable} from '@angular/core';

import {StompService} from '@elderbyte/ngx-stomp';
import {SortDir} from '../models/SortDir';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ApiService} from './ApiService';
import {Observable, BehaviorSubject} from 'rxjs';
import {Block} from '../models/Block';
import {map} from 'rxjs/operators';

@Injectable()
export class BlockService {
  private latestBlockSource = new BehaviorSubject<Block>(null);
  latestBlock$ = this.latestBlockSource.asObservable();

  constructor(private stompService: StompService, private http: HttpClient) {
    this.listenForBlocks();
  }

  public getBlocks(limit: number = 10,
                   offset: number = 0,
                   minerId: number = -1,
                   sortBy: string,
                   sortDir: SortDir = SortDir.ASC,
                   emptyBlocks: boolean = true): Observable<Block[]> {
    return this.http.get(
      minerId === -1
        ? ApiService.API_ENDPOINT + '/blocks'
        : ApiService.API_ENDPOINT + '/miners/' + minerId + '/blocks',
      {
        params: new HttpParams()
          .set('sortBy', sortBy)
          .set('sortDir', String(sortDir))
          .set('limit', String(limit))
          .set('offset', String(offset))
          .set('emptyBlocks', String(emptyBlocks))
      }
    ).pipe(map(
      response => <Block[]>response
    ));
  }

  public getBlock(height: number): Observable<Block> {
    return this.http.get(
      ApiService.API_ENDPOINT + '/blocks/' + height
    ).pipe(map(
      response => <Block>response
    ));
  }

  private listenForBlocks() {
    this.stompService.connectedClient.subscribe(client => {
      const sub = client.subscribe('/stream/blocks');

      sub.messages.subscribe(message => {
        this.latestBlockSource.next(<Block>message.bodyJson);
      });
    }, err => {
      console.error('STOMP: Failed to subscribe to blocks stream!', err);
    });
  }
}
