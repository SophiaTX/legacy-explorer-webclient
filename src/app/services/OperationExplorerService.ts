import {BehaviorSubject} from 'rxjs/index';
import {Operation} from '../models/Operation';
import {AccountsPair} from '../models/AccountsPair';
import {AccountService} from './AccountService';
import {Injectable} from '@angular/core';
import {OperationType} from '../models/OperationType';
import {LogicalOperator} from '../models/LogicalOperator';
import {OperationService} from './OperationService';
import {NumberRange} from '../models/NumberRange';
import {SortDir} from '../models/SortDir';
import {Account} from '../models/Account';

@Injectable()
export class OperationExplorerService {
  private readonly operations: BehaviorSubject<Operation[]>;
  private readonly selectedTypes: BehaviorSubject<OperationType[]>;
  private readonly selectedAccounts: BehaviorSubject<AccountsPair>;
  private readonly selectedHeightRange: BehaviorSubject<NumberRange>;
  private pageSize: number;
  private pageIndex: BehaviorSubject<number>;
  private loading: boolean;

  private readonly operationCount: BehaviorSubject<number>;
  private error: string;

  private readonly senderAccountValid: BehaviorSubject<boolean>;
  private readonly targetAccountValid: BehaviorSubject<boolean>;

  constructor(
    private accountService: AccountService,
    private operationService: OperationService
  ) {
    this.operations = new BehaviorSubject<Operation[]>([]);
    this.selectedAccounts = new BehaviorSubject<AccountsPair>(new AccountsPair());
    this.selectedTypes = new BehaviorSubject<OperationType[]>([]);
    this.selectedHeightRange = new BehaviorSubject<NumberRange>(new NumberRange());
    this.operationCount = new BehaviorSubject<number>(0);
    this.pageSize = 10;
    this.pageIndex = new BehaviorSubject<number>(0);
    this.loading = false;

    this.senderAccountValid = new BehaviorSubject<boolean>(true);
    this.targetAccountValid = new BehaviorSubject<boolean>(true);

    // TODO: somehow debounce this
    this.selectedAccounts.subscribe(event => this.onFilterChange());
    this.selectedTypes.subscribe(event => this.onFilterChange());
    this.selectedHeightRange.subscribe(event => this.onFilterChange());
  }

  private onFilterChange() {
    this.pageIndex.next(0);
    this.updateOperations();
  }

  public setPageSize(size: number) {
    this.pageSize = size;
  }

  public getPageSize(): number {
    return this.pageSize;
  }

  public setPageIndex(index: number) {
    this.pageIndex.next(index);
  }

  public getPageIndex(): BehaviorSubject<number> {
    return this.pageIndex;
  }

  public updateSelectedTypes(types: OperationType[]) {
    this.selectedTypes.next(types);
  }

  public updateSenderAccount(senderAccount: string) {
    const accountsPair = this.selectedAccounts.getValue();
    if (senderAccount.length === 0) {
      accountsPair.sender = null;
      this.selectedAccounts.next(accountsPair);
      this.senderAccountValid.next(true);
    } else {
      this.getAccount(senderAccount).then(account => {
        accountsPair.sender = account;
        this.selectedAccounts.next(accountsPair);
        this.senderAccountValid.next(true);
      }).catch(e => {
        accountsPair.sender = null;
        this.selectedAccounts.next(accountsPair);
        this.senderAccountValid.next(false);
      });
    }
  }

  public updateTargetAccount(targetAccount: string) {
    const accountsPair = this.selectedAccounts.getValue();
    if (targetAccount.length === 0) {
      accountsPair.target = null;
      this.selectedAccounts.next(accountsPair);
      this.targetAccountValid.next(true);
    } else {
      this.getAccount(targetAccount).then(accountId => {
        accountsPair.target = accountId;
        this.selectedAccounts.next(accountsPair);
        this.targetAccountValid.next(true);
      }).catch(e => {
        accountsPair.target = null;
        this.selectedAccounts.next(accountsPair);
        this.targetAccountValid.next(false);
      });
    }
  }

  public updateHeightRange(heightRange: NumberRange) {
    this.selectedHeightRange.next(heightRange);
  }

  public updateOperator(operator: LogicalOperator) {
    const accountsPair = this.selectedAccounts.getValue();
    accountsPair.operator = operator;
    this.selectedAccounts.next(accountsPair);
  }

  getOperations(): BehaviorSubject<Operation[]> {
    return this.operations;
  }

  getSelectedTypes(): BehaviorSubject<OperationType[]> {
    return this.selectedTypes;
  }

  getSelectedAccounts(): BehaviorSubject<AccountsPair> {
    return this.selectedAccounts;
  }

  getSelectedHeightRange(): BehaviorSubject<NumberRange> {
    return this.selectedHeightRange;
  }

  isLoading(): boolean {
    return this.loading;
  }

  getOperationCount(): BehaviorSubject<number> {
    return this.operationCount;
  }

  getError(): string {
    return this.error;
  }

  getSenderAccountValid() {
    return this.senderAccountValid;
  }

  getTargetAccountValid() {
    return this.targetAccountValid;
  }

  getAccount(account: string): Promise<Account> {
    if (account.length < 1) {
      return Promise.reject('Empty input');
    }

    const accountIdMatch = account.match(/^(?:1\.2\.)?([0-9]+)$/);
    if (accountIdMatch) {
      return this.accountService.getById(parseInt(accountIdMatch[1], 10));
    } else if (/^[0-9a-z-_]+$/i.test(account)) {
      return this.accountService.getByName(account);
    } else {
      return Promise.reject('Invalid account ID format');
    }
  }

  public updateOperations(): void {
    this.loading = true;

    this.operationService.countOperations(
      this.selectedAccounts.getValue(),
      this.selectedTypes.getValue()
    ).subscribe(count => {
      this.operationCount.next(count);
    });

    this.operationService.getOperations(
      this.pageSize,
      this.pageSize * this.pageIndex.getValue(),
      SortDir.DESC,
      this.selectedAccounts.getValue(),
      this.selectedTypes.getValue(),
      this.selectedHeightRange.getValue()
    ).subscribe(operations => {
      this.operations.next(<Operation[]>operations);
      this.error = '';
      this.loading = false;
    }, error => {
      this.operations.next([]);
      this.error = error.status === 422
        ? 'Server considers the query to be invalid, check your input'
        : 'Failed to load operations from the server, try again later';
      this.loading = false;
    });
  }

  private reset() {
    this.operationCount.next(0);
    this.operations.next([]);
    this.selectedTypes.next([]);
    this.selectedAccounts.next(new AccountsPair());
    this.error = undefined;
    this.loading = false;
    this.updateOperations();
  }
}
