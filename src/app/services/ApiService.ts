import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Miner} from '../models/Miner';
import {Transaction} from '../models/Transaction';
import {SortDir} from '../models/SortDir';
import {Observable} from 'rxjs';
import {DynamicGlobalProperties} from '../models/DynamicGlobalProperties';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable()
export class ApiService {
  public static HOST = environment.serverHost;
  public static API_ENDPOINT = ApiService.HOST + '/api';
  public static STOMP_ENDPOINT = ApiService.HOST + '/stomp';

  constructor(public http: HttpClient) {
  }

  // TODO: move these methods to their own, separate services

  public getTransactions(
    blockHeight: number,
    limit: number = 10,
    offset: number = 0,
    sortDir: SortDir = SortDir.ASC
  ): Observable<Transaction[]> {
    return this.http.get(
      ApiService.API_ENDPOINT + '/blocks/' + blockHeight + '/transactions',
      {
        params: new HttpParams()
          .set('sortDir', String(sortDir))
          .set('limit', String(limit))
          .set('offset', String(offset))
      }
    ).pipe(map(
      response => <Transaction[]>response
    ));
  }

  public getTransaction(blockHeight: number, txOrder: number) {
    return this.http.get(
      ApiService.API_ENDPOINT + '/transactions/B' + blockHeight + 'T' + txOrder
    ).pipe(map(
      response => <Transaction>response
    ));
  }

  public getMiners(
    limit: number,
    offset: number,
    sortBy: string = 'id',
    sortDir: SortDir = SortDir.ASC
  ): Observable<Miner[]> {
    return this.http.get(ApiService.API_ENDPOINT + '/miners', {
      params: new HttpParams()
        .set('sortBy', sortBy)
        .set('sortDir', String(sortDir))
        .set('limit', String(limit))
        .set('offset', String(offset))
    }).pipe(map(
      response => <Miner[]>response
    ));
  }

  public getMinerCount(): Observable<number> {
    return this.http.get(ApiService.API_ENDPOINT + '/miners/count').pipe(map(
      response => (<any>response).count
    ));
  }

  public getMiner(minerId: number): Observable<Miner> {
    return this.http.get(ApiService.API_ENDPOINT + '/miners/' + minerId).pipe(map(
      response => <Miner>response
    ));
  }

  public getDynamicGlobalProperties(): Observable<DynamicGlobalProperties> {
    return this.http.get(ApiService.API_ENDPOINT + '/var/dynamicGlobalProperties').pipe(map(
      response => <DynamicGlobalProperties>response
    ));
  }
}
