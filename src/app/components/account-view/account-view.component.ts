import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Account} from '../../models/Account';
import {AccountService} from '../../services/AccountService';

@Component({
  selector: 'account-view',
  templateUrl: './account-view.component.html',
  styleUrls: ['./account-view.component.scss']
})
export class AccountViewComponent implements OnInit {
  public account: Account;
  public accountId: number;
  public error: string;

  constructor(private accountService: AccountService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.account = null;
      this.accountId = params['id'];

      this.accountService.getById(this.accountId).then(account => {
        this.account = account;
      }).catch(error => {
        if (error.status === 404) {
          this.error = 'Requested account 1.2.' + this.accountId + ' was not found';
        } else {
          this.error = 'There was an error loading data about the account';
        }
      });
    });
  }
}
