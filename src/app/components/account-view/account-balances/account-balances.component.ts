import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {Balance} from '../../../models/Balance';
import {AccountService} from '../../../services/AccountService';

@Component({
  selector: 'account-balances',
  templateUrl: './account-balances.component.html',
  styleUrls: ['./account-balances.component.scss']
})
export class AccountBalancesComponent implements OnInit, OnChanges {
  public balances: Balance[];
  public dataSource = new MatTableDataSource<Balance>();
  public visibleColumns = ['resource', 'amount'];
  public error: string;

  @Input() accountId: number;

  constructor(private accountService: AccountService) {
  }

  ngOnInit(): void {
    this.loadBalances();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['accountId'].previousValue !== undefined) {
      this.reset();
      this.loadBalances();
    }
  }

  private reset(): void {
    this.balances = undefined;
    this.error = undefined;
    this.dataSource = new MatTableDataSource<Balance>();
  }

  private loadBalances(): void {
    this.accountService.getAccountBalances(this.accountId).then(accountBalances => {
      this.balances = accountBalances;
      this.dataSource.data = this.balances;
    }).catch(error => {
      if (error.status === 404) {
        this.error = 'No such account';
      } else {
        this.error = 'There was an error loading balances';
      }
    });
  }
}
