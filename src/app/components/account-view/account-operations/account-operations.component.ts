import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {Operation} from '../../../models/Operation';
import {DataViewComponent} from '../../data-view/data-view.component';
import {OperationType} from '../../../models/OperationType';
import {OperationService} from '../../../services/OperationService';
import {AccountsPair} from '../../../models/AccountsPair';
import {LogicalOperator} from '../../../models/LogicalOperator';
import {SortDir} from '../../../models/SortDir';
import {Account} from '../../../models/Account';

@Component({
  selector: 'account-operations',
  templateUrl: './account-operations.component.html',
  styleUrls: ['./account-operations.component.scss']
})
export class AccountOperationsComponent implements OnInit, OnChanges {
  public operationCount = 0;
  public operations: Operation[];
  public dataSource = new MatTableDataSource<Operation>();
  public visibleColumns = ['type', 'block', 'sender', 'target', 'asset+amount', 'view'];
  public error: string;
  public operationTypes = Object.keys(OperationType);
  public selectedOpTypes = this.operationTypes;

  public loading = false;

  @Input() account: Account;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private operationService: OperationService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.paginator.pageSize = 5;

    this.paginator.page.subscribe(() => {
      this.updateOperations();
    });

    this.updateOperations();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['accountId'].previousValue !== undefined) {
      this.reset();
    }
  }

  public selectNone() {
    this.selectedOpTypes = [];
    this.updateOperations();
  }

  public selectAll() {
    this.selectedOpTypes = this.operationTypes;
    this.updateOperations();
  }

  public updateOperations(): void {
    this.loading = true;

    const accountsPair = <AccountsPair>{
      sender: this.account,
      target: this.account,
      operator: LogicalOperator.OR
    };

    this.operationService.countOperations(
      accountsPair,
      <OperationType[]>this.selectedOpTypes
    ).subscribe(
      count => this.operationCount = count
    );

    this.operationService.getOperations(
      this.paginator.pageSize,
      this.paginator.pageSize * this.paginator.pageIndex,
      SortDir.DESC,
      accountsPair,
      <OperationType[]>this.selectedOpTypes
    ).subscribe(operations => {
      this.operations = operations;
      this.dataSource.data = operations;
      this.loading = false;
    }, error => {
      if (this.account && error.status === 404) {
        this.error = 'No such account';
      } else {
        this.error = 'Failed to load operations from the server, try again later';
      }
    });
  }

  private reset() {
    this.operationCount = 0;
    this.operations = undefined;
    this.dataSource.data = [];
    this.error = undefined;
    this.selectedOpTypes = this.operationTypes;
    this.loading = false;
    this.updateOperations();
  }

  public viewData(data: Object): void {
    this.dialog.open(DataViewComponent, {
      width: '1100px',
      data: {data: data, title: 'Operation data'}
    });
  }
}
