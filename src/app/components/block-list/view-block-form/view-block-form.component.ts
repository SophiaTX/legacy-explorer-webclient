import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'view-block-form',
  templateUrl: './view-block-form.component.html'
})
export class ViewBlockFormComponent {
  private blockHeight = '';

  constructor(private router: Router, private snackBar: MatSnackBar) {
  }

  public onInput(event) {
    const val = event.target.value;
    if (!isNaN(val) || val.length === 0) {
      this.blockHeight = val;
    } else {
      event.target.value = this.blockHeight;
    }
  }

  viewBlock() {
    if (this.blockHeight != '')
      this.router.navigate(['/blocks/' + this.blockHeight]);
    else
      this.snackBar.open("Invalid block height!", null, {duration:3000});
  }
}
