import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Block} from '../../models/Block';
import {MatSort, MatSortable, MatTableDataSource, Sort} from '@angular/material';
import {SortDir} from '../../models/SortDir';
import {BlockService} from '../../services/BlockService';
import {Subscription} from 'rxjs';

@Component({
  selector: 'block-list',
  templateUrl: './block-list.component.html',
  styleUrls: ['./block-list.component.scss']
})
export class BlockListComponent implements OnInit, OnDestroy, OnChanges {
  private static BLOCKS_PER_REQUEST = 50;
  private static INITIAL_BLOCK_COUNT = 10;

  public blocks: Block[] = [];
  public dataSource = new MatTableDataSource<Block>();
  public visibleColumns = ['height', 'transactionCount', 'operationCount', 'timestamp'];

  private offset = 0;
  public initialLoading = true;
  public loading = false;
  public emptyBlocks = true;
  public reachedEndOfResults = false;
  public error: string;
  public loadMoreError = false;

  private latestBlockSubscription: Subscription;

  @Input() minerId: number;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private blockService: BlockService) {
  }

  ngOnInit(): void {
    // only display miner column if this is not a miner-specific block list view
    if (!this.minerId) {
      this.visibleColumns.push('miner', 'view');
    } else {
      this.visibleColumns.push('view');
    }

    // initialize sort with default sort parameters
    this.sort.sort(<MatSortable>{
      id: 'height',
      start: 'desc'
    });

    // if sort parameters change, we need to re-fetch the data
    this.sort.sortChange.subscribe((sort: Sort) => {
      this.reset();
    });

    this.loadInitialBlocks();

    this.latestBlockSubscription = this.blockService.latestBlock$
      .subscribe((block) => this.onBlockReceived(block));
  }

  ngOnDestroy(): void {
    this.latestBlockSubscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if minerId changes ( not undefined previously - which would mean init ),
    // we need to reset the data and re-fetch the table
    if (changes['minerId'].previousValue !== undefined) {
      this.reset();
    }
  }

  private onBlockReceived(block: Block) {
    // first subscription is always null
    if (block == null) {
      return;
    }

    // if we should only display blocks with at least one transaction
    if (!this.emptyBlocks && block.transactionCount < 1) {
      return;
    }

    // if we should only display blocks for specific miner
    if (this.minerId !== undefined && this.minerId !== block.miner.id) {
      return;
    }

    // we only append blocks when sorting by height is enabled
    if (this.sort.active == 'height') {
      if (this.sort.direction == 'desc') {
        // if the direction is descending, we can just show the block on the top
        // as a latest block ( desc = from newest to oldest )
        this.offset += 1;
        this.blocks.unshift(block);
        // remove last block to prevent the stack from getting too large
        this.blocks = this.blocks.slice(0, -1);
        if (!this.initialLoading) {
          this.updateTable();
        }
      } else {
        // we can not display it otherwise as we would have to append it to the bottom
        // which can be millions of blocks "below" the oldest block we are showing
        // so lets just accommodate for the offset change new block causes
        this.offset -= 1;
      }
    }
  }

  private loadInitialBlocks(): void {
    this.blockService.getBlocks(
      BlockListComponent.INITIAL_BLOCK_COUNT,
      this.offset,
      this.minerId,
      this.sort.active,
      <SortDir>SortDir[this.sort.direction.toUpperCase()],
      this.emptyBlocks
    ).subscribe(blocks => {
      this.appendBlocks(blocks);
    }, error => {
      if (this.minerId && error.status === 404) {
        this.error = 'No such miner';
      } else {
        this.error = 'Failed to load blocks from the server, try again later';
      }
    });
  }

  private reset(): void {
    this.blocks = [];
    this.offset = 0;
    this.initialLoading = true;
    this.loading = false;
    this.reachedEndOfResults = false;
    this.error = undefined;
    this.updateTable();
    this.loadInitialBlocks();
  }

  protected loadMore(): void {
    if (!this.loading) {
      this.loading = true;
      this.loadMoreError = false;
      this.blockService.getBlocks(
        BlockListComponent.BLOCKS_PER_REQUEST,
        this.offset,
        this.minerId,
        this.sort.active,
        <SortDir>SortDir[this.sort.direction.toUpperCase()],
        this.emptyBlocks
      ).subscribe(blocks => {
          this.appendBlocks(blocks);
          this.loading = false;
        },
        error => {
          this.loadMoreError = true;
          this.loading = false;
        }
      );
    }
  }

  private appendBlocks(blocks: Block[]): void {
    // the initial load has lower block count and we need to accommodate
    // for that in order to be able to properly handle "end of results" state.
    if (this.initialLoading) {
      this.initialLoading = false;
      if (blocks.length < BlockListComponent.INITIAL_BLOCK_COUNT) {
        this.reachedEndOfResults = true;
      }
    } else {
      if (blocks.length < BlockListComponent.BLOCKS_PER_REQUEST) {
        this.reachedEndOfResults = true;
      }
    }

    this.offset += blocks.length;
    this.blocks = this.blocks.concat(blocks);
    this.updateTable();
  }

  private updateTable(): void {
    this.dataSource.data = this.blocks;
  }

  public toggleEmptyBlocks(): void {
    this.emptyBlocks = !this.emptyBlocks;
    this.reset(); // re-fetch
  }
}
