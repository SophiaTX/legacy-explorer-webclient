import {Component, Input} from '@angular/core';
import {Transaction} from '../../models/Transaction';
import {MatDialog} from '@angular/material';
import {DataViewComponent} from '../data-view/data-view.component';
import {Operation} from '../../models/Operation';

@Component({
  selector: 'transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent {
  @Input() index: number;
  @Input() transaction: Transaction;

  public visibleColumns = ['type', 'sender', 'target', 'asset', 'amount', 'view'];

  constructor(private dialog: MatDialog) {
  }

  public viewTxSignatures(): void {
    this.dialog.open(DataViewComponent, {
      width: '1000px',
      data: {
        data: this.transaction.signatures,
        title: 'Signatures for TX #' + this.transaction.orderInBlock
      }
    });
  }

  public viewOperationData(o: Operation): void {
    this.dialog.open(DataViewComponent, {
      width: '1000px',
      data: {
        data: o.data,
        title: 'Data for operation #' + o.orderInTX + ' of TX #' + this.transaction.orderInBlock
      }
    });
  }
}
