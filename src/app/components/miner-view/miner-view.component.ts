import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/ApiService';
import {ActivatedRoute} from '@angular/router';
import {Miner} from '../../models/Miner';

@Component({
  selector: 'miner-view',
  templateUrl: './miner-view.component.html',
  styleUrls: ['./miner-view.component.scss']
})
export class MinerViewComponent implements OnInit {
  public miner: Miner;
  public minerId: number;
  public error: string;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.miner = null;
      this.minerId = params['id'];

      this.apiService.getMiner(this.minerId).subscribe(miner => {
        this.miner = miner;
      }, error => {
        if (error.status === 404) {
          this.error = 'Requested miner 1.4.' + this.minerId + ' was not found';
        } else {
          this.error = 'There was an error loading data about the miner';
        }
      });
    });
  }
}
