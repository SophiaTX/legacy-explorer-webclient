import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatSortable, MatTableDataSource, Sort} from '@angular/material';
import {Account} from '../../models/Account';
import {SortDir} from '../../models/SortDir';
import {AccountService} from '../../services/AccountService';

@Component({
  selector: 'account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {
  public accountCount = 0;
  public dataSource = new MatTableDataSource<Account>();
  public visibleColumns = ['id', 'name', 'mainBalance', 'registrar', 'miner', 'view'];
  public error: string;

  public loading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private accountService: AccountService) {
  }

  ngOnInit(): void {
    this.paginator.pageSize = 10;
    this.sort.sort(<MatSortable>{
      id: 'mainBalance',
      start: 'desc',
    });

    this.updateAccounts();
    this.accountService.getAccountCount().subscribe(count => this.accountCount = count);
    this.paginator.page.subscribe(() => {
      this.updateAccounts();
    });
    this.sort.sortChange.subscribe((sort: Sort) => {
      this.updateAccounts();
    });
  }

  private updateAccounts() {
    this.loading = true;
    this.accountService.getAccounts(
      this.paginator.pageSize,
      this.paginator.pageSize * this.paginator.pageIndex,
      this.sort.active,
      <SortDir>SortDir[this.sort.direction.toUpperCase()]
    ).then(accounts => {
      this.dataSource.data = accounts;
      this.loading = false;
    }).catch(error => {
      this.error = 'Failed to load accounts from the server, try again later';
    });
  }
}
