import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {AccountService} from '../../../services/AccountService';

@Component({
  selector: 'view-account-form',
  templateUrl: './view-account-form.component.html'
})
export class ViewAccountFormComponent {
  public account = '';
  public isLoading = false;

  constructor(
    private accountService: AccountService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
  }

  viewAccount() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    const accountIdMatch = this.account.match(/^(?:1\.2\.)?([0-9]+)$/);
    if (accountIdMatch) {
      this.redirectById(parseInt(accountIdMatch[1]));
    } else if (/^[0-9a-zA-Z-_]+$/.test(this.account)) {
      this.redirectByName();
    } else {
      this.snackBar.open('Invalid account ID!', null, {duration: 3000});
      this.isLoading = false;
    }
  }

  redirectById(accId: number) {
    this.accountService.getById(accId).then(a => {
      this.router.navigateByUrl('/accounts/' + a.id);
    }).catch(e => {
      this.snackBar.open('Account not found', null, {duration: 3000});
      this.isLoading = false;
    });
  }

  redirectByName() {
    this.accountService.getByName(this.account).then(a => {
      this.router.navigateByUrl('/accounts/' + a.id);
    }).catch(e => {
      this.snackBar.open('Account not found', null, {duration: 3000});
      this.isLoading = false;
    });
  }
}
