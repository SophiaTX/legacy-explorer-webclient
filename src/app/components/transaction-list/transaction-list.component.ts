import {Component, Input, OnInit} from '@angular/core';
import {ApiService} from '../../services/ApiService';
import {Transaction} from '../../models/Transaction';

@Component({
  selector: 'transaction-list',
  templateUrl: './transaction-list.component.html'
})
export class TransactionListComponent implements OnInit {
  private static TX_PER_REQ = 50;

  public transactions: Transaction[];
  private offset = 0;
  public loading = false;
  public reachedEndOfResults = false;
  public error;

  @Input() blockHeight: number;

  constructor(private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.loadMore();
  }

  protected loadMore() {
    if (!this.loading) {
      this.loading = true;
      this.apiService.getTransactions(
        this.blockHeight,
        TransactionListComponent.TX_PER_REQ,
        this.offset
      ).subscribe(transactions => {
        // \/ just so we know when txs are loaded first without using another flag
        if (!this.transactions) this.transactions = [];
        this.appendTransactions(transactions);
        this.loading = false;
      }, error => {
        if (error.status === 404) {
          this.error = 'Transactions could not be displayed because the block was not found';
        } else {
          this.error = 'There was an error loading transactions';
        }
      });
    }
  }

  private appendTransactions(transactions: Transaction[]): void {
    if (transactions.length < TransactionListComponent.TX_PER_REQ) {
      this.reachedEndOfResults = true;
    }
    this.offset += transactions.length;
    this.transactions = this.transactions.concat(transactions);
  }
}
