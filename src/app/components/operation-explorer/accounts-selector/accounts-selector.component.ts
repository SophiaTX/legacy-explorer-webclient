import {Component} from '@angular/core';
import {LogicalOperator} from '../../../models/LogicalOperator';
import {OperationExplorerService} from '../../../services/OperationExplorerService';
import {FormControl, FormGroup, FormGroupDirective, NgForm} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {AccountsPair} from '../../../models/AccountsPair';

/** Always show errors */
export class InstantErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return (control && control.invalid);
  }
}

@Component({
  selector: 'accounts-selector',
  templateUrl: './accounts-selector.component.html',
  styleUrls: ['./accounts-selector.component.scss']
})
export class AccountsSelectorComponent {
  public static readonly STR_ANYONE = 'anyone';
  public static readonly STR_INVALID_FORMAT = 'Invalid format';
  public static readonly STR_NO_SUCH_ACCOUNT = 'No such account';
  public static readonly ACCOUNT_INPUT_DEBOUNCE_TIME = 500;
  public static readonly ACCOUNT_FORMAT = /^(?:(?:1\.2\.)?[0-9]+|[0-9a-z-_]+)$/i;

  public operators = Object.keys(LogicalOperator);
  public selectedOperator: LogicalOperator = LogicalOperator.AND;
  public matcher = new InstantErrorStateMatcher();

  public accountsForm = new FormGroup({
    senderAccount: new FormControl(),
    targetAccount: new FormControl()
  });

  public matchedSenderAccount = AccountsSelectorComponent.STR_ANYONE;
  public matchedTargetAccount = AccountsSelectorComponent.STR_ANYONE;

  public senderAccountError = '';
  public targetAccountError = '';

  constructor(private service: OperationExplorerService) {
    // Listener for user input, debounce, validation, push to service
    this.accountsForm.controls['senderAccount'].valueChanges
      .debounceTime(AccountsSelectorComponent.ACCOUNT_INPUT_DEBOUNCE_TIME)
      .subscribe(value => {
        if (AccountsSelectorComponent.ACCOUNT_FORMAT.test(value) || value.length === 0) {
          this.service.updateSenderAccount(value);
        } else {
          this.senderAccountError = AccountsSelectorComponent.STR_INVALID_FORMAT;
          this.accountsForm.controls['senderAccount'].setErrors({'f': true});
        }
      });

    this.accountsForm.controls['targetAccount'].valueChanges
      .debounceTime(AccountsSelectorComponent.ACCOUNT_INPUT_DEBOUNCE_TIME)
      .subscribe(value => {
        if (AccountsSelectorComponent.ACCOUNT_FORMAT.test(value) || value.length === 0) {
          this.service.updateTargetAccount(value);
        } else {
          this.targetAccountError = AccountsSelectorComponent.STR_INVALID_FORMAT;
          this.accountsForm.controls['targetAccount'].setErrors({'f': true});
        }
      });

    // Listen to lookup result from service ( whether the accounts were found )
    service.getSenderAccountValid().subscribe(v => {
      this.accountsForm.controls['senderAccount'].setErrors(v ? null : {'v': true});
      if (!v) {
        this.senderAccountError = AccountsSelectorComponent.STR_NO_SUCH_ACCOUNT;
      }
    });

    service.getTargetAccountValid().subscribe(v => {
      this.accountsForm.controls['targetAccount'].setErrors(v ? null : {'v': true});
      if (!v) {
        this.targetAccountError = AccountsSelectorComponent.STR_NO_SUCH_ACCOUNT;
      }
    });

    // Display matched results ( account names )
    service.getSelectedAccounts().subscribe(accountsPair => {
      this.processState(accountsPair);
    });

    // Load initial state from service
    this.initState(service.getSelectedAccounts().getValue());
  }

  initState(accountsPair: AccountsPair) {
    if (accountsPair.sender !== null) {
      this.accountsForm.controls['senderAccount'].setValue(accountsPair.sender.name);
    }
    if (accountsPair.target !== null) {
      this.accountsForm.controls['targetAccount'].setValue(accountsPair.target.name);
    }
  }

  processState(accountsPair: AccountsPair) {
    if (accountsPair.sender == null) {
      this.matchedSenderAccount = AccountsSelectorComponent.STR_ANYONE;
    } else {
      const account = accountsPair.sender;
      this.matchedSenderAccount = account.name + ' ( 1.2.' + account.id + ' )';
    }
    if (accountsPair.target == null) {
      this.matchedTargetAccount = AccountsSelectorComponent.STR_ANYONE;
    } else {
      const account = accountsPair.target;
      this.matchedTargetAccount = account.name + ' ( 1.2.' + account.id + ' )';
    }
  }

  public updateOperator() {
    this.service.updateOperator(this.selectedOperator);
  }
}
