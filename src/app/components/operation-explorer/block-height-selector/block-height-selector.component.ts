import {Component, OnInit} from '@angular/core';
import {OperationExplorerService} from '../../../services/OperationExplorerService';
import {FormControl, FormGroup, FormGroupDirective, NgForm} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import 'rxjs/add/operator/debounceTime';
import {NumberRange} from '../../../models/NumberRange';

/** Always show errors */
export class InstantErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return (control && control.invalid);
  }
}

@Component({
  selector: 'block-height-selector',
  templateUrl: './block-height-selector.component.html',
  styleUrls: ['./block-height-selector.component.scss']
})
export class BlockHeightSelectorComponent {
  private static readonly HEIGHT_INPUT_DEBOUNCE_TIME = 500;

  public matcher = new InstantErrorStateMatcher();

  public blockHeightForm = new FormGroup({
    from: new FormControl(),
    to: new FormControl()
  });

  public range = new NumberRange();
  private rawRange = new NumberRange();

  constructor(private service: OperationExplorerService) {
    this.blockHeightForm.controls['from'].valueChanges
      .debounceTime(BlockHeightSelectorComponent.HEIGHT_INPUT_DEBOUNCE_TIME)
      .subscribe(value => {
        if (this.processFromInput(value)) {
          this.processToInput(this.rawRange.to);
          this.service.updateHeightRange(this.range);
        }
      });

    this.blockHeightForm.controls['to'].valueChanges
      .debounceTime(BlockHeightSelectorComponent.HEIGHT_INPUT_DEBOUNCE_TIME)
      .subscribe(value => {
        if (this.processToInput(value)) {
          this.processFromInput(this.rawRange.from);
          this.service.updateHeightRange(this.range);
        }
      });

    const heightRange = this.service.getSelectedHeightRange().getValue();
    this.blockHeightForm.controls['from'].setValue(heightRange.from);
    this.blockHeightForm.controls['to'].setValue(heightRange.to);
  }

  private processFromInput(value): boolean {
    value = value === '' ? 0 : value;
    this.rawRange.from = value;
    if (
      (value !== null && this.range.to !== null && value > this.range.to) ||
      (value !== null && value < 0)
    ) {
      this.blockHeightForm.controls['from'].setErrors({'invalid': true});
      return false;
    } else {
      this.range.from = value;
      this.blockHeightForm.controls['from'].setErrors(null);
      return true;
    }
  }

  private processToInput(value): boolean {
    value = value === '' ? null : value;
    this.rawRange.to = value;
    if (
      (value !== null && this.range.from !== null && value < this.range.from) ||
      (value !== null && value < 0)
    ) {
      this.blockHeightForm.controls['to'].setErrors({'invalid': true});
      return false;
    } else {
      this.range.to = value;
      this.blockHeightForm.controls['to'].setErrors(null);
      return true;
    }
  }

  getMatchText(): string {
    if (this.range.from === null && this.range.to === null) {
      return 'all blocks';
    }
    if (this.range.from !== null && this.range.to !== null) {
      return 'blocks ' + this.range.from + ' → ' + this.range.to;
    }
    if (this.range.from !== null) {
      return 'block ' + this.range.from;
    }
    if (this.range.to !== null) {
      return 'block ' + this.range.to;
    }
  }
}
