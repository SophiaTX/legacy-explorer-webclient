import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {Operation} from '../../models/Operation';
import {DataViewComponent} from '../data-view/data-view.component';
import {OperationExplorerService} from '../../services/OperationExplorerService';

@Component({
  selector: 'operation-explorer',
  templateUrl: './operation-explorer.component.html',
  styleUrls: ['./operation-explorer.component.scss']
})
export class OperationExplorerComponent implements OnInit {
  public dataSource = new MatTableDataSource<Operation>();
  public totalRows: number = 0;
  public visibleColumns = ['block', 'tx', 'op', 'type', 'sender', 'target', 'asset', 'amt', 'view'];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public service: OperationExplorerService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.paginator.pageSize = this.service.getPageSize();

    this.service.getPageIndex().subscribe(pageIndex => {
      this.paginator.pageIndex = pageIndex;
    });

    this.paginator.page.subscribe(() => {
      this.service.setPageIndex(this.paginator.pageIndex);
      this.service.setPageSize(this.paginator.pageSize);
      this.service.updateOperations();
    });

    this.service.updateOperations();

    this.service.getOperations().subscribe(operations => this.dataSource.data = operations);
    this.service.getOperationCount().subscribe(count => {
      this.totalRows = count;
    });
  }

  public viewData(data: Object): void {
    this.dialog.open(DataViewComponent, {
      width: '1100px',
      data: {data: data, title: 'Operation data'}
    });
  }
}
