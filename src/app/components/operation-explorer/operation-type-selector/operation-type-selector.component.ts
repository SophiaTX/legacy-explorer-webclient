import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {OperationType} from '../../../models/OperationType';
import {MatCheckboxChange} from '@angular/material';
import {OperationExplorerService} from '../../../services/OperationExplorerService';

@Component({
  selector: 'operation-type-selector',
  templateUrl: './operation-type-selector.component.html',
  styleUrls: ['./operation-type-selector.component.scss']
})
export class OperationTypeSelectorComponent implements OnInit, OnChanges {
  public OPERATION_TYPES = {
    'Tokens': [
      OperationType.TRANSFER,
      OperationType.WITHDRAW_PERMISSION_CREATE,
      OperationType.WITHDRAW_PERMISSION_UPDATE,
      OperationType.WITHDRAW_PERMISSION_DELETE,
      OperationType.WITHDRAW_PERMISSION_CLAIM,
    ],
    'Mining': [
      OperationType.MINER_CREATE,
      OperationType.MINER_UPDATE,
      OperationType.MINER_UPDATE_GLOBAL_PARAMETERS,
    ],
    'Accounts': [
      OperationType.ACCOUNT_CREATE,
      OperationType.ACCOUNT_UPDATE,
    ],
    'Proposals': [
      OperationType.PROPOSAL_CREATE,
      OperationType.PROPOSAL_UPDATE,
      OperationType.PROPOSAL_DELETE
    ],
    'Vesting': [
      OperationType.VESTING_BALANCE_CREATE,
      OperationType.VESTING_BALANCE_WITHDRAW
    ],
    'Subscriptions': [
      OperationType.SUBSCRIBE,
      OperationType.SUBSCRIBE_BY_AUTHOR,
      OperationType.AUTOMATIC_RENEWAL_OF_SUBSCRIPTION,
      OperationType.DISALLOW_AUTOMATIC_RENEWAL_OF_SUBSCRIPTION,
      OperationType.RENEWAL_OF_SUBSCRIPTION
    ],
    'Assets': [
      OperationType.ASSET_CREATE,
      OperationType.ASSET_ISSUE,
      OperationType.ASSET_RESERVE,
      OperationType.ASSET_CLAIM_FEES,
      OperationType.ASSET_FUND_POOLS,
      OperationType.ASSET_PUBLISH_FEED,
      OperationType.UPDATE_MONITORED_ASSET,
      OperationType.UPDATE_USER_ISSUED_ASSET
    ],
    'Other': [
      OperationType.CUSTOM,
      OperationType.ASSERT
    ],
    'DCT': [
      OperationType.CONTENT_SUBMIT,
      OperationType.CONTENT_CANCELLATION,
      OperationType.SET_PUBLISHING_MANAGER,
      OperationType.SET_PUBLISHING_RIGHT,
      OperationType.REQUEST_TO_BUY,
      OperationType.LEAVE_RATING_AND_COMMENT,
      OperationType.READY_TO_PUBLISH,
      OperationType.PROOF_OF_CUSTODY,
      OperationType.DELIVER_KEYS,
      OperationType.REPORT_STATS,
      OperationType.READY_TO_PUBLISH2,
      OperationType.RETURN_ESCROW_SUBMISSION,
      OperationType.RETURN_ESCROW_BUYING,
      OperationType.PAY_SEEDER,
      OperationType.FINISH_BUYING
    ]
  };
  public operationTypesSizes = {};
  public state = {};
  public selectedTypes = [];

  constructor(private operationExplorerService: OperationExplorerService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnInit(): void {
    this.buildOperationTypeSizesObject();
    this.operationExplorerService.getSelectedTypes().subscribe(st => {
      this.selectedTypes = st;
      this.buildStateObject();
    });
  }

  onMasterCheckboxChanged(changes: MatCheckboxChange) {
    const category = changes.source.id;
    const categoryCheckboxes = this.OPERATION_TYPES[category];

    if (changes.checked) {
      this.state[category] = this.operationTypesSizes[category];
      categoryCheckboxes.forEach((type: string) => {
        if (!this.selectedTypes.includes(type)) {
          this.selectedTypes.push(type);
        }
      });
    } else {
      this.state[category] = 0;
      this.OPERATION_TYPES[category].forEach((type: string) => {
        this.selectedTypes = this.selectedTypes.filter(item => item !== type);
      });
    }

    this.operationExplorerService.updateSelectedTypes(this.selectedTypes);
  }

  onCheckboxStateChanged(changes: MatCheckboxChange) {
    if (changes.checked) {
      this.state[this.getCheckboxCategory(changes.source.id)]++;
      this.selectedTypes.push(changes.source.id);
    } else {
      this.state[this.getCheckboxCategory(changes.source.id)]--;
      this.selectedTypes = this.selectedTypes.filter(item => item !== changes.source.id);
    }

    this.operationExplorerService.updateSelectedTypes(this.selectedTypes);
  }

  getCheckboxCategory(checkbox): string {
    for (const category of Object.keys(this.OPERATION_TYPES)) {
      const enums = this.OPERATION_TYPES[category];
      if (enums.includes(OperationType[checkbox])) {
        return category;
      }
    }
  }

  buildStateObject() {
    for (const category of Object.keys(this.OPERATION_TYPES)) {
      let totalSelected = 0;
      for (const type of Object.values(this.OPERATION_TYPES[category])) {
        if (this.selectedTypes.includes(type)) {
          totalSelected++;
        }
      }

      this.state[category] = totalSelected;
    }
  }

  buildOperationTypeSizesObject() {
    for (const category of Object.keys(this.OPERATION_TYPES)) {
      const enums = this.OPERATION_TYPES[category];
      this.operationTypesSizes[category] = enums.length;
    }
  }
}
