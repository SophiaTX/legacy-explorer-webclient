import {Component, Inject, Input} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'data-view',
  templateUrl: './data-view.component.html',
  styleUrls: ['./data-view.component.scss']
})
export class DataViewComponent {
  constructor(private dialogRef: MatDialogRef<DataViewComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ModalData) {
  }

  close(): void {
    this.dialogRef.close();
  }
}

class ModalData {
  public title;
  public data;
}
