import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../../services/ApiService';
import {MatPaginator, MatSort, MatSortable, MatTableDataSource, Sort} from '@angular/material';
import {Miner} from '../../models/Miner';
import {SortDir} from '../../models/SortDir';

@Component({
  selector: 'miner-list',
  templateUrl: './miner-list.component.html',
  styleUrls: ['./miner-list.component.scss']
})
export class MinerListComponent implements OnInit {
  public minerCount = 0;
  public dataSource = new MatTableDataSource<Miner>();
  public visibleColumns = ['id', 'account', 'totalVotes', 'totalMissed', 'view'];
  public error: string;

  public loading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.paginator.pageSize = 10;
    this.sort.sort(<MatSortable>{
      id: 'id',
      start: 'asc'
    });

    this.paginator.page.subscribe(() => {
      this.updateMiners();
    });

    this.sort.sortChange.subscribe((sort: Sort) => {
      this.updateMiners();
    });

    this.updateMiners();
    this.apiService.getMinerCount().subscribe(count => this.minerCount = count);
  }

  private updateMiners(): void {
    this.loading = true;
    this.apiService.getMiners(
      this.paginator.pageSize,
      this.paginator.pageSize * this.paginator.pageIndex,
      this.sort.active,
      <SortDir>SortDir[this.sort.direction.toUpperCase()]
    ).subscribe(miners => {
      this.dataSource.data = miners;
      this.loading = false;
    }, error => {
      this.error = 'Failed to load miners from the server, try again later';
    });
  }
}
