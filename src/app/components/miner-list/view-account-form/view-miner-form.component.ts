import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'view-miner-form',
  templateUrl: './view-miner-form.component.html'
})
export class ViewMinerFormComponent {
  private miner = '';

  constructor(private router: Router, private snackBar: MatSnackBar) {
  }

  public onInput(event) {
    const val = event.target.value;
    if (/[.0-9]/.test(val) || val.length === 0) {
      this.miner = val;
    } else {
      event.target.value = this.miner;
    }
  }

  viewMiner() {
    if (/^1\.4\.[0-9]+$/.test(this.miner)) {
      this.router.navigateByUrl('/miners/' + this.miner.split('\.')[2]);
    } else if(/^[0-9]+$/.test(this.miner)) {
      this.router.navigateByUrl('/miners/' + this.miner);
    } else {
      this.snackBar.open("Invalid miner ID!", null, {duration:3000});
    }
  }
}
