import {Component, OnInit} from '@angular/core';
import {Block} from '../../models/Block';
import {ActivatedRoute} from '@angular/router';
import {MatDialog} from '@angular/material';
import {DataViewComponent} from '../data-view/data-view.component';
import {BlockService} from '../../services/BlockService';

@Component({
  selector: 'block-view',
  templateUrl: './block-view.component.html',
  styleUrls: ['./block-view.component.scss']
})
export class BlockViewComponent implements OnInit {
  public block: Block;
  public blockHeight: number;
  public error: string;

  constructor(private blockService: BlockService, private route: ActivatedRoute, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.block = null;
      this.blockHeight = params['height'];

      this.blockService.getBlock(this.blockHeight).subscribe(block => {
          this.block = block;
        }, error => {
          if (error.status === 404) {
            this.error = 'Requested block ' + this.blockHeight + ' was not found';
          } else {
            this.error = 'There was an error loading block, try again later';
          }
        }
      );
    });
  }

  public viewMinerSignature() {
    this.dialog.open(DataViewComponent, {
      width: '1000px',
      data: {data: this.block.minerSignature, title: 'Miner signature'}
    });
  }
}
