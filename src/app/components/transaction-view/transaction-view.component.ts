import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Transaction} from '../../models/Transaction';
import {ApiService} from '../../services/ApiService';

@Component({
  selector: 'transaction-view',
  templateUrl: './transaction-view.component.html',
  styleUrls: ['./transaction-view.component.scss']
})
export class TransactionViewComponent implements OnInit {
  public transaction: Transaction = null;
  public blockHeight: number;
  public txOrder: number;
  public error: string;
  public loading = false;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {
  }

  public ngOnInit() {
    this.route.params.subscribe(params => {
      this.blockHeight = params['block'];
      this.txOrder = params['tx'];
      this.transaction = null;
      this.loading = true;
      this.apiService.getTransaction(this.blockHeight, this.txOrder).subscribe(tx => {
        this.transaction = tx;
        this.loading = false;
      }, error => {
        if (error.status = 404) {
          this.error = 'Requested transaction was not found';
        } else {
          this.error = 'Failed to load transaction from the server, try again later';
        }
        this.loading = false;
      });
    });
  }
}
