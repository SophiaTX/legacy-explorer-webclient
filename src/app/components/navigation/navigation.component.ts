import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @Input() items: NavigationItem[];

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.router.events.subscribe((url: any) => {
      if (!url.url)
        return;

      const activeUrl = url.urlAfterRedirects ? url.urlAfterRedirects : url.url;

      this.items.forEach(item => {
        item.active = String(activeUrl).startsWith(item.endpoint);
      });
    });
  }

  selectItem(index) {
    this.items.forEach(item => item.active = false);
    this.items[index].active = true;
    this.router.navigateByUrl(this.items[index].endpoint);
  }
}

export class NavigationItem {
  public title;
  public endpoint;
  public active;

  constructor(title, endpoint) {
    this.title = title;
    this.endpoint = endpoint;
    this.active = false;
  }
}
