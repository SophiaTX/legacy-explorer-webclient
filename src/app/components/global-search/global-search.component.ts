import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {AccountService} from '../../services/AccountService';

@Component({
  selector: 'global-search',
  templateUrl: './global-search.component.html',
  styleUrls: ['./global-search.component.scss']
})
export class GlobalSearchComponent {
  private static readonly MATCHINGS = {
    'account': /^1\.2\.([0-9]+)$/,
    'miner': /^1\.4\.([0-9]+)$/,
    'block': /^([0-9]+)$/
  };

  public value: string;

  constructor(
    private accountService: AccountService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
  }

  public search() {
    let staticMatchFound = false;
    for (const type in GlobalSearchComponent.MATCHINGS) {
      const matches = this.value.match(GlobalSearchComponent.MATCHINGS[type]);

      if (matches) {
        this.router.navigateByUrl('/' + type + 's/' + matches[1]);
        staticMatchFound = true;
        break;
      }
    }

    if (!staticMatchFound) {
      const matches = this.value.match(/^B([0-9]+)T([0-9]+)(?:O([0-9]+))?$/i);
      if (matches) {
        this.router.navigateByUrl('/tx/' + matches[1] + '/' + matches[2]);
        return;
      }

      if (/^[a-z][a-z0-9-_]+$/i.test(this.value)) {
        this.accountService.getByName(this.value).then(account => {
          this.router.navigateByUrl('/accounts/' + account.id);
        }).catch(e => {
          this.snackBar.open('No such account', null, {duration: 3000});
        });
        return;
      }

      this.snackBar.open('Invalid input', null, {duration: 3000});
    }
  }
}
