import {Component, OnDestroy, OnInit} from '@angular/core';
import {DynamicGlobalProperties} from '../../models/DynamicGlobalProperties';
import {ApiService} from '../../services/ApiService';

@Component({
  selector: 'blockchain-stats',
  templateUrl: './blockchain-stats.component.html',
  styleUrls: ['./blockchain-stats.component.scss']
})
export class BlockchainStatsComponent implements OnInit, OnDestroy {
  public dgp: DynamicGlobalProperties;
  public error: boolean = false;
  private intervalId;

  constructor(private apiService: ApiService) {
  }

  private updateStats(): void {
    this.apiService.getDynamicGlobalProperties().subscribe(
      dgp => {
        this.dgp = dgp;
        this.error = false;
      }, error => {
        this.error = true;
      }
    );
  }

  ngOnInit(): void {
    this.updateStats();
    this.intervalId = setInterval(() => {
      this.updateStats();
    }, 5000);
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalId);
  }
}
