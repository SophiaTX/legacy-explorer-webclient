import {Miner} from './Miner';

export class Block {
  public height: number;
  public time: string;
  public miner: Miner;
  public minerSignature: string;
  public transactionCount: number;
  public operationCount: number;

  constructor(height: number, time: string, miner: Miner, minerSignature: string, transactionCount: number, operationCount: number) {
    this.height = height;
    this.time = time;
    this.miner = miner;
    this.minerSignature = minerSignature;
    this.transactionCount = transactionCount;
    this.operationCount = operationCount;
  }
}
