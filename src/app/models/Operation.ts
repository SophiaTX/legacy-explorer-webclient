import {OperationType} from './OperationType';

export class Operation {
  public id: String;
  public type: OperationType;
  public data: Object;
  public orderInTX: number;
  public sender: Account;
  public target: Account;
  public blockHeight: number;
  public orderOfTxInBlock: number;

  constructor(id: String, type: OperationType, data: Object, orderInTX: number, sender: Account, target: Account, blockHeight: number, orderOfTxInBlock: number) {
    this.id = id;
    this.type = type;
    this.data = data;
    this.orderInTX = orderInTX;
    this.sender = sender;
    this.target = target;
    this.blockHeight = blockHeight;
    this.orderOfTxInBlock = orderOfTxInBlock;
  }
}
