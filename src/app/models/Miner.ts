import {Account} from './Account';

export class Miner {
  public id: number;
  public totalVotes: number;
  public totalMissed: number;
  public account: Account;

  constructor(id: number, totalVotes: number, totalMissed: number, account: Account) {
    this.id = id;
    this.totalVotes = totalVotes;
    this.totalMissed = totalMissed;
    this.account = account;
  }
}
