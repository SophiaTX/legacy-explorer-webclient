export class Balance {
  public resourceId;
  public amount;

  constructor(resource, balance) {
    this.resourceId = resource;
    this.amount = balance;
  }
}
