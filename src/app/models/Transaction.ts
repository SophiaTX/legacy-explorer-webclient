import {Operation} from './Operation';

export class Transaction {
  public id: string;
  public refBlockNum: number;
  public refBlockPrefix: number;
  public signatures: String[];
  public operations: Operation[];
  public orderInBlock: number;

  constructor(id: string, refBlockNum: number, refBlockPrefix: number, signatures: String[], operations: Operation[], orderInBlock: number) {
    this.id = id;
    this.refBlockNum = refBlockNum;
    this.refBlockPrefix = refBlockPrefix;
    this.signatures = signatures;
    this.operations = operations;
    this.orderInBlock = orderInBlock;
  }
}
