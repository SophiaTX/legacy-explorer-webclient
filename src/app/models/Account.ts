export class Account {
  public id: number;
  public name: string;
  public registrarId: number;
  public minerId: number;
  public mainBalance: number;

  constructor(id: number, name: string, registrarId: number, minerId: number, mainBalance: number) {
    this.id = id;
    this.name = name;
    this.registrarId = registrarId;
    this.minerId = minerId;
    this.mainBalance = mainBalance;
  }
}
