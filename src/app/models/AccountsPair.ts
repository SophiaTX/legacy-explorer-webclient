import {LogicalOperator} from './LogicalOperator';
import {Account} from './Account';

export class AccountsPair {
  public sender: Account = null;
  public target: Account = null;
  public operator: LogicalOperator = LogicalOperator.AND;
}
