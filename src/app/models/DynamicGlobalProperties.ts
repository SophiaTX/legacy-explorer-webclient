export class DynamicGlobalProperties {
  public headBlockNumber: number;
  public headBlockId: string;
  public time: string;
  public currentMiner: string;
  public nextMaintenanceTime: string;
  public lastBudgetTime: string;
  public unspentFeeBudget: number;
  public minedRewards: number;
  public minerBudgetFromFees: number;
  public minerBudgetFromRewards: number;
  public accountsRegisteredThisInterval: number;
  public recentlyMissedCount: number;
  public currentAslot: number;
  public recentSlotsFilled: string;
  public dynamicFlags: number;
  public lastIrreversibleBlockNum: number;

  constructor(headBlockNumber: number, headBlockId: string, time: string, currentMiner: string, nextMaintenanceTime: string, lastBudgetTime: string, unspentFeeBudget: number, minedRewards: number, minerBudgetFromFees: number, minerBudgetFromRewards: number, accountsRegisteredThisInterval: number, recentlyMissedCount: number, currentAslot: number, recentSlotsFilled: string, dynamicFlags: number, lastIrreversibleBlockNum: number) {
    this.headBlockNumber = headBlockNumber;
    this.headBlockId = headBlockId;
    this.time = time;
    this.currentMiner = currentMiner;
    this.nextMaintenanceTime = nextMaintenanceTime;
    this.lastBudgetTime = lastBudgetTime;
    this.unspentFeeBudget = unspentFeeBudget;
    this.minedRewards = minedRewards;
    this.minerBudgetFromFees = minerBudgetFromFees;
    this.minerBudgetFromRewards = minerBudgetFromRewards;
    this.accountsRegisteredThisInterval = accountsRegisteredThisInterval;
    this.recentlyMissedCount = recentlyMissedCount;
    this.currentAslot = currentAslot;
    this.recentSlotsFilled = recentSlotsFilled;
    this.dynamicFlags = dynamicFlags;
    this.lastIrreversibleBlockNum = lastIrreversibleBlockNum;
  }
}
