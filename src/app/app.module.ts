import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {AccountListComponent} from './components/account-list/account-list.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ApiService} from './services/ApiService';
import {StompModule} from '@elderbyte/ngx-stomp';
import {BlockService} from './services/BlockService';
import {ViewBlockFormComponent} from './components/block-list/view-block-form/view-block-form.component';
import {BlockViewComponent} from './components/block-view/block-view.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';
import {CommonModule} from '@angular/common';
import {TransactionComponent} from './components/transaction/transaction.component';
import {TransactionListComponent} from './components/transaction-list/transaction-list.component';
import {DataViewComponent} from './components/data-view/data-view.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.page';
import {ViewAccountFormComponent} from './components/account-list/view-account-form/view-account-form.component';
import {BlockListComponent} from './components/block-list/block-list.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {AccountViewComponent} from './components/account-view/account-view.component';
import {AccountBalancesComponent} from './components/account-view/account-balances/account-balances.component';
import {MinerListComponent} from './components/miner-list/miner-list.component';
import {ViewMinerFormComponent} from './components/miner-list/view-account-form/view-miner-form.component';
import {MinerViewComponent} from './components/miner-view/miner-view.component';
import {BlockchainStatsComponent} from './components/blockchain-stats/blockchain-stats.component';
import {AccountOperationsComponent} from './components/account-view/account-operations/account-operations.component';
import {OperationExplorerComponent} from './components/operation-explorer/operation-explorer.component';
import {KeysPipe} from './pipes/KeysPipe';
import {OperationTypeSelectorComponent} from './components/operation-explorer/operation-type-selector/operation-type-selector.component';
import {AccountsSelectorComponent} from './components/operation-explorer/accounts-selector/accounts-selector.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AccountService} from './services/AccountService';
import {OperationExplorerService} from './services/OperationExplorerService';
import {OperationService} from './services/OperationService';
import {BlockHeightSelectorComponent} from './components/operation-explorer/block-height-selector/block-height-selector.component';
import {GlobalSearchComponent} from './components/global-search/global-search.component';
import {TransactionViewComponent} from './components/transaction-view/transaction-view.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/blocks', pathMatch: 'full'},
  {path: 'blocks', component: BlockListComponent},
  {path: 'blocks/:height', component: BlockViewComponent},
  {path: 'accounts', component: AccountListComponent},
  {path: 'accounts/:id', component: AccountViewComponent},
  {path: 'miners', component: MinerListComponent},
  {path: 'miners/:id', component: MinerViewComponent},
  {path: 'operations', component: OperationExplorerComponent},
  {path: 'tx/:block/:tx', component: TransactionViewComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    KeysPipe,
    AppComponent,
    BlockListComponent,
    ViewBlockFormComponent,
    BlockViewComponent,
    TransactionComponent,
    TransactionListComponent,
    DataViewComponent,
    PageNotFoundComponent,
    ViewAccountFormComponent,
    AccountListComponent,
    NavigationComponent,
    AccountViewComponent,
    AccountBalancesComponent,
    MinerListComponent,
    ViewMinerFormComponent,
    MinerViewComponent,
    BlockchainStatsComponent,
    AccountOperationsComponent,
    OperationExplorerComponent,
    OperationTypeSelectorComponent,
    AccountsSelectorComponent,
    BlockHeightSelectorComponent,
    GlobalSearchComponent,
    TransactionViewComponent
  ],
  entryComponents: [
    DataViewComponent
  ],
  imports: [
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    StompModule.forRoot({
      endpointUrl: ApiService.STOMP_ENDPOINT,
      withSockJs: true
    }),

    // angular material
    BrowserModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatTooltipModule,
    MatTabsModule,
    MatRippleModule,
    MatSnackBarModule,
    MatSortModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    HttpClient,
    ApiService,
    BlockService,
    AccountService,
    OperationService,
    OperationExplorerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
