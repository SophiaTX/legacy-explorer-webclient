import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {NavigationItem} from './components/navigation/navigation.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(private router: Router){}

  navItems = [
    new NavigationItem('Blocks', '/blocks'),
    new NavigationItem('Accounts', '/accounts'),
    new NavigationItem('Miners', '/miners'),
    new NavigationItem('Operation Explorer', '/operations')
  ];

  public goHome(): void {
    this.router.navigate(['/']);
  }
}
