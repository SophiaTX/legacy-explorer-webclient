# SophiaTX Legacy Blockchain Explorer

**The support for this project ended on May 31st, 2018.**

**This software is NOT compatible with the production version of SophiaTX
blockchain!**

This repository is intended for developers who are working on the software
based around the legacy blockchain. If you find any bugs, feel free to fix
them and open merge request.

---

Angular web application that allows end-users to browse contents of blockchain. 

Requires explorer server to operate.

### Environments

**production** environment is used when the fully built app is a part of 
explorer server's static content served over HTTP from root directory 
( eg. `explorer.tld` ). For production, "/" ( same host as the user is on ) 
is used as an explorer server host and either `/api` or `/stomp` is appended
to result in `explorer.tld/api` or `explorer.tld/stomp` respectively. 

This means that paths starting with `/api` or `/stomp` are reserved by
the explorer server itself and so are any other paths with dot in them -
this is needed so the files the Angular app itself consists of are served
properly and not redirected to `/` ( `index.html` ) like the other routes are.

Like already mentioned, other paths are redirected to `index.html` internally 
( similar to Apache's mod-rewrite ) to be handled by the Angular application 
in the browser of the end user.

**development** environment expects that the app is served by the Angular development 
server ( `ng serve` ) and an explorer server is running on the same machine on port 8080.
`http://localhost:8080/api` and `http://localhost:8080/stomp` are used. 
